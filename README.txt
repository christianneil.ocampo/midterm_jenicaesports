Course and Section: CSCI 40 - E

Group Members:
    Chan, Lex Philip Gabriel D. | ID:211411
    Dagdad, Eldon C.| ID:211764
    Francisco, Daniella Joan T. | ID:212531
    Ocampo, Christianneil Emmanuel D.Y. | ID:214293
    Vizmanos, Jenica Alea B. | ID:216351

Project Title: Widget v2

Members' App Assignments:
    Dashboard - Vizmanos
    Announcement - Chan
    Forum - Dagdag
    Assignment - Francisco
    Calendar - Ocampo

Date of Submission: May 11, 2023

Statement: This lab is done solely by our group, and all guiding information 
           used in creating the submission was sourced from the CSCI 40 
           Canvas modules. Supplementary information were sourced from the references below.

References:
> Midterm (Widget v1)
Foreign key from one app to another:
https://stackoverflow.com/questions/323763/foreign-key-from-one-app-into-another-in-django

Django model fields guide:
https://docs.djangoproject.com/en/4.1/ref/models/fields/

Save computed value in a model field:
https://stackoverflow.com/questions/30586994/django-model-save-computed-value-in-a-model-field

Date-Time formatting:
https://www.programiz.com/python-programming/datetime
https://stackoverflow.com/questions/3743222/how-do-i-convert-a-datetime-to-date
https://stackoverflow.com/questions/45262667/convert-datetime-to-time-in-python
https://stackoverflow.com/questions/1759455/how-can-i-account-for-period-am-pm-using-strftime

> Finals (Widget v2)
Bootstrap CSS Styling:
https://pypi.org/project/django-bootstrap5/

FBV Guide:
https://django.cowhite.com/blog/function-based-views-in-django/

Modifying Model Forms:
https://docs.djangoproject.com/en/4.2/topics/forms/modelforms/#django.forms.ModelForm
https://docs.djangoproject.com/en/4.2/topics/db/models/

Git Ignore Cleaning:
https://stackoverflow.com/questions/1139762/ignore-files-that-have-already-been-committed-to-a-git-repository

CBV Detail View Guide:
https://stackoverflow.com/questions/41287431/django-combine-detailview-and-listview

(sgd) Lex Philip Gabriel D. Chan, 05/11/2023
(sgd) Eldon C. Dagdag, 05/11/2023
(sgd) Daniella Joan T. Francisco, 05/11/2023
(sgd) Christianneil Emmanuel D.Y. Ocampo, 05/11/2023
(sgd) Jenica Alea B. Vizmanos, 05/11/2023