from typing import Any, Dict
from django.shortcuts import render
from .models import ForumPost, Reply
from django.views.generic import DetailView, CreateView, UpdateView

def ForumPostList(request):
    posts = ForumPost.objects.all()
    context = {
        'posts': posts
        }
    return render(request, "forum/forum.html", context)

class ForumPostDetailView(DetailView):
    model = ForumPost
    template_name = 'forum/forumpost-details.html'
    fields = '__all__'

    def get_context_data(self,  *args, **kwargs):
        context = super(ForumPostDetailView, self).get_context_data(*args, **kwargs)
        context['reply_list'] = Reply.objects.all()
        return context

class ForumPostAddView(CreateView):
    model = ForumPost
    template_name = 'forum/forumpost-add.html'
    fields = '__all__'

class ForumPostUpdateView(UpdateView):
    model = ForumPost
    template_name = 'forum/forumpost-edit.html'
    fields = '__all__'
