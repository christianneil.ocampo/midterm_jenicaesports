from django.db import models
from dashboard import models as dashboard_models
from django.urls import reverse

class ForumPost(models.Model):
    title = models.CharField(max_length=255, default='')
    body = models.TextField(default='')
    author = models.ForeignKey(dashboard_models.WidgetUser, on_delete=models.CASCADE)
    pub_datetime = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return '{} by {} on {}'.format(self.title, self.author, self.pub_datetime.strftime("%m/%d/%Y %I:%M %p"))
    
    def get_absolute_url(self):
        return reverse('forum:forumpost-details', kwargs={'pk':self.pk})
    
    def formatted_date(self):
        return '{}'.format(self.pub_datetime.strftime("%m/%d/%y"))
    
    def formatted_time(self):
        return '{}'.format(self.pub_datetime.strftime("%I:%M %p"))

class Reply(models.Model):
    body = models.TextField(default='')
    author = models.ForeignKey(dashboard_models.WidgetUser, on_delete=models.CASCADE)
    pub_datetime = models.DateTimeField(auto_now=False, auto_now_add=False)
    forum_post = models.ForeignKey(ForumPost, on_delete=models.CASCADE, default='')

    def __str__(self):
        return 'Reply by {} on {}'.format(self.author, self.pub_datetime.strftime("%m/%d/%Y %I:%M %p"))

    def get_absolute_url(self):
        return reverse('forum:reply-details', kwargs={'pk':self.pk})
    
    def formatted_date(self):
        return '{}'.format(self.pub_datetime.strftime("%m/%d/%y"))
    
    def formatted_time(self):
        return '{}'.format(self.pub_datetime.strftime("%I:%M %p"))