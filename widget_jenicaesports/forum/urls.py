from django.urls import path
from .views import ForumPostList, ForumPostDetailView, ForumPostAddView, ForumPostUpdateView

urlpatterns = [
    path('', ForumPostList, name='forum'),
    path('forumposts/add/', ForumPostAddView.as_view(), name='forumpost-add'),
    path('forumposts/<int:pk>/edit/', ForumPostUpdateView.as_view(), name='forumpost-edit'),
    path('forumposts/<int:pk>/details/', ForumPostDetailView.as_view(), name='forumpost-details'),
]

app_name="forum"