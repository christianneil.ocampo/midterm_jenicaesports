from django.contrib import admin
from .models import Reply, ForumPost

class ReplyInline(admin.TabularInline):
    model = Reply

class ForumPostAdmin(admin.ModelAdmin):
    model = ForumPost
    search_fields = ('title', 'author',)
    list_display = ('title', 'author',)
    list_filter = ('title', 'author',)
    inlines = [ReplyInline]

admin.site.register(Reply)
admin.site.register(ForumPost, ForumPostAdmin)