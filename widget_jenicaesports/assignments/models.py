from django.db import models
from django.urls import reverse

# Create your models here.
class Course(models.Model):
    code = models.CharField(max_length=10)
    title = models.CharField(max_length=255)
    section = models.CharField(max_length=3)

    def format_course(self):
        return '{} {} - {}'.format(self.code, self.title, self.section)

    def __str__(self):
        return '{}-{}'.format(self.code, self.section)


class Assignment(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    perfect_score = models.IntegerField()
    passing_score = models.IntegerField(editable=False)

    def save(self, *args, **kwargs):
        self.passing_score = int((self.perfect_score)*(0.60))
        super(Assignment, self).save(*args, **kwargs)

    def format_assignment(self):
        return '{}'.format(self.name)

    def __str__(self):
        return '{} {}: {}'.format(self.course.code, self.course.section, self.name)

    def get_absolute_url(self):
        return reverse('assignments:assignment-details', kwargs={'pk': self.pk})