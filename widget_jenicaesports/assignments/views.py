from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from .models import Assignment


def assignments_view(request):
    tasks = Assignment.objects.all()
    context = {'tasks' : tasks}
    return render(request, 'assignments/assignments.html', context)


class AssignmentDetailView(DetailView):
    model = Assignment
    template_name = 'assignments/assignment-details.html'


class AssignmentCreateView(CreateView):
    model = Assignment
    fields = '__all__'
    template_name = 'assignments/assignment-add.html'


class AssignmentUpdateView(UpdateView):
    model = Assignment
    fields = '__all__'
    template_name = 'assignments/assignment-edit.html'