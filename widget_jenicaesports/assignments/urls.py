from django.urls import path

from .views import assignments_view, AssignmentDetailView, AssignmentCreateView, AssignmentUpdateView

urlpatterns = [
    path('', assignments_view, name='assignments'),
    path('<int:pk>/details/', AssignmentDetailView.as_view(), name='assignment-details'),
    path('add/', AssignmentCreateView.as_view(), name='assignment-add'),
    path('<int:pk>/edit/', AssignmentUpdateView.as_view(), name='assignment-edit'),
]

app_name = 'assignments' 