from django.contrib import admin

from .models import Course, Assignment


class CourseAdmin(admin.ModelAdmin):
    model = Course
    search_fields = ('code', 'title', 'section')
    list_display = ('code', 'title', 'section')


class AssignmentAdmin(admin.ModelAdmin):
    model = Assignment
    search_fields = ('name', 'description', 'course')
    list_display = ('name', 'course')


admin.site.register(Course, CourseAdmin)
admin.site.register(Assignment, AssignmentAdmin)