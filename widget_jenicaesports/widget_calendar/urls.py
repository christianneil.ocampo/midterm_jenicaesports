from django.urls import path
from .views import (
    calendar, EventDetailsView, EventsCreateView, EventsUpdateView
)

urlpatterns = [
    path('', calendar, name='calendar home'),
    path('events/<pk>/details/', EventDetailsView.as_view(), name='event-details'),
    path('events/add/', EventsCreateView.as_view(),name='add-event'),
    path('events/<pk>/edit/',EventsUpdateView.as_view(),name='update-event'),
]

app_name = "widget_calendar"