from django.shortcuts import render
from django.views.generic.detail import DetailView
#from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from .models import Event

def calendar(request):
    activities = Event.objects.all()
    context = {
        'activities': activities
    }
    return render(request, 'widget_calendar/calendar.html', context)

class EventDetailsView(DetailView):    
    model = Event
    template_name = 'widget_calendar/event-details.html'

class EventsCreateView(CreateView):
    model = Event
    fields = '__all__'
    template_name = "widget_calendar/event-add.html"

class EventsUpdateView(UpdateView):
    model = Event
    fields = '__all__'
    template_name = "widget_calendar/event-edit.html"