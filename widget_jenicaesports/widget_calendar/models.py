from django.db import models
from django.utils.translation import gettext_lazy as _
from assignments import models as assignments_models
from django.urls import reverse

class Location(models.Model):

    class Modality(models.TextChoices):
        onsite = 'onsite', _('Onsite')
        online = 'online', _('Online')
        hybrid = 'hybrid', _('Hybrid')

    mode = models.CharField(
        max_length=6,
        choices=Modality.choices,
        default=Modality.onsite
    )
    venue = models.TextField(default='')

    def __str__(self):
        return '{} - {}'.format(self.mode, self.venue)

class Event(models.Model):
    activity = models.CharField(default='', max_length=255)
    target_datetime = models.DateTimeField(default='')
    estimated_hours = models.FloatField(default='')
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    course = models.ForeignKey(assignments_models.Course, on_delete=models.CASCADE)
    
    def __str__(self):
        return '{} on {} at {}'.format(
            self.activity,
            self.target_datetime.strftime("%m/%d/%y"),
            self.target_datetime.strftime("%I:%M %p"),
            )
    
    def formatted_course(self):
        return '{} {}-{}'.format(self.course.code, self.course.title, self.course.section)

    def formatted_date(self):
        return '{}'.format(self.target_datetime.strftime("%m/%d/%y"))
    
    def formatted_time(self):
        return '{}'.format(self.target_datetime.strftime("%I:%M %p"))
    
    def get_absolute_url(self):
        return reverse('widget_calendar:event-details', kwargs={'pk': self.pk})