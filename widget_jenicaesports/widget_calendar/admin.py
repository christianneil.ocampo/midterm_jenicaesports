from django.contrib import admin
from .models import Event, Location

class EventAdmin(admin.ModelAdmin):
    model = Event
    search_fields = ('activity', 'course',)
    list_display = ('activity', 'target_datetime','estimated_hours','location','course',)
    list_filter = ('activity','location','course',)

class LocationAdmin(admin.ModelAdmin):
    model = Location
    search_fields = ('venue','mode',)
    list_display = ('venue', 'mode',)
    list_filter = ('venue',)

admin.site.register(Event, EventAdmin)
admin.site.register(Location, LocationAdmin)