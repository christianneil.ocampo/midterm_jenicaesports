from django.db import models
from django.urls import reverse

class Department(models.Model):
    dept_name = models.CharField(max_length=255, default='')
    home_unit = models.CharField(max_length=255, default='')
    
    def __str__(self):
        return '{}, {}'.format(self.dept_name, self.home_unit)

class WidgetUser(models.Model):
    first_name = models.CharField(max_length=255, default='', verbose_name='First Name')
    middle_name = models.CharField(max_length=255, default='', verbose_name='Middle Name')
    last_name = models.CharField(max_length=255, default='', verbose_name='Last Name')
    department = models.ForeignKey(Department, on_delete=models.CASCADE, verbose_name='Department, Home Unit')
    
    def __str__(self):
        return '{}, {}'.format(self.last_name, self.first_name)
    
    def get_absolute_url(self):
        return reverse('dashboard:widgetuser-details', kwargs={'pk': self.pk})
