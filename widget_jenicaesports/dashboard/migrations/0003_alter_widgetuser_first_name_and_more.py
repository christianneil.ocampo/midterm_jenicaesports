# Generated by Django 4.1.6 on 2023-05-10 07:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("dashboard", "0002_alter_widgetuser_department"),
    ]

    operations = [
        migrations.AlterField(
            model_name="widgetuser",
            name="first_name",
            field=models.CharField(
                default="", max_length=255, verbose_name="First Name"
            ),
        ),
        migrations.AlterField(
            model_name="widgetuser",
            name="last_name",
            field=models.CharField(
                default="", max_length=255, verbose_name="Last Name"
            ),
        ),
        migrations.AlterField(
            model_name="widgetuser",
            name="middle_name",
            field=models.CharField(
                default="", max_length=255, verbose_name="Middle Name"
            ),
        ),
    ]
