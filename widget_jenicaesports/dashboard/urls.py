from django.urls import path
from .views import (
    dashboard_view, WidgetUserDetailView, WidgetUserCreateView, WidgetUserUpdateView
)

urlpatterns = [
    path('', dashboard_view, name='dashboard'),
    path('widgetusers/<int:pk>/details/', WidgetUserDetailView.as_view(), name='widgetuser-details'),
    path('widgetusers/add/', WidgetUserCreateView.as_view(), name='widgetuser-add'),
    path('widgetusers/<int:pk>/edit/', WidgetUserUpdateView.as_view(), name='widgetuser-edit'),
]

app_name = 'dashboard'