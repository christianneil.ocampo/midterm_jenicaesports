from django.contrib import admin
from .models import Announcement, Reaction

class ReactionInLine(admin.TabularInline):
    model = Reaction

class AnnouncementAdmin(admin.ModelAdmin):
    model = Announcement
    inlines = [ReactionInLine]

admin.site.register(Reaction)
admin.site.register(Announcement, AnnouncementAdmin)
