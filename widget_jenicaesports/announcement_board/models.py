from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from dashboard import models as dashboard_models

class Announcement(models.Model):
    title = models.CharField(max_length=255, default="")
    body = models.TextField(default="")
    author = models.ForeignKey(dashboard_models.WidgetUser, on_delete=models.CASCADE)
    pub_datetime = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return '{} by {} {} published on {}, {}: {}'.format(
            self.title, 
            self.author.first_name, 
            self.author.last_name, 
            self.pub_datetime.strftime("%m/%d/%y"),
            self.pub_datetime.strftime("%I:%M %p"),
            self.body
        )
    
    def formatted_date(self):
        return '{}'.format(self.pub_datetime.strftime("%m/%d/%y"))

    def formatted_time(self):
        return '{}'.format(self.pub_datetime.strftime("%I:%M %p"))
    
    def getLikeReactions(self):
        try:
            return '{}'.format(Reaction.objects.get(name="Like", announcement=self).tally)
        
        except:
            return '0'
    
    def getLoveReactions(self):
        try:
            return '{}'.format(Reaction.objects.get(name="Love", announcement=self).tally)
        
        except:
            return '0'
    
    def getAngryReactions(self):
        try:
            return '{}'.format(Reaction.objects.get(name="Angry", announcement=self).tally)
        
        except:
            return '0'
    
    def get_absolute_url(self):
        return reverse('announcement_board:announcement-details', kwargs={'pk': self.pk})

class Reaction(models.Model):
    announcement = models.ForeignKey(Announcement, on_delete=models.CASCADE, related_name='reactions')
    
    class Reacts(models.TextChoices):
        like = 'Like', _('Like')
        love = 'Love', _('Love')
        angry = 'Angry', _('Angry')

    name = models.CharField(
        max_length=5,
        choices=Reacts.choices,
        default=Reacts.like
    )

    tally = models.IntegerField(default=0)
    
    def __str__(self):
        return '{} reaction on {}'.format(self.name, self.announcement)