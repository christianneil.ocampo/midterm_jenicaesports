from django.shortcuts import render, redirect
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.http import HttpResponse

from .models import Announcement, Reaction

def announcements(request):
    return render(request, 'announcement_board/announcements.html', {'announcements':Announcement.objects.order_by('-pub_datetime')})

class AnnouncementDetailView(DetailView):
    model = Announcement
    template_name = 'announcement_board/announcement-details.html'

class AnnouncementCreateView(CreateView):
    model = Announcement
    fields = 'title', 'body', 'author'
    template_name = 'announcement_board/announcement-add.html'

class AnnouncementUpdateView(UpdateView):
    model = Announcement
    fields = 'title', 'body', 'author'
    template_name = 'announcement_board/announcement-edit.html'